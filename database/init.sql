-- Crie um banco de dados
CREATE DATABASE mydatabase;

-- Use o banco de dados
\c copadatabase;

-- Users
CREATE TABLE users (
    id serial PRIMARY KEY,
    name VARCHAR (255),
    email VARCHAR (255),
);

CREATE TABLE cards (
    id serial PRIMARY KEY,
    name VARCHAR (255),
    icon VARCHAR (255),
);
