FROM alpine

# Base dependencies
RUN apk update
RUN apk add npm
RUN apk add nginx
RUN mkdir -p /var/www/html /var/www/html/images
RUN rm /etc/nginx/nginx.conf

# Copies the html index and conf file into the container
COPY nginx.conf /etc/nginx
COPY index.html /var/www/html

# 
RUN chown nginx:nginx /var/www/html
RUN chown nginx:nginx /var/www/html/index.html
RUN chmod 644 /var/www/html/index.html

# Set any environment variables
# ENV <key> <value>

# Expose any ports that the container will listen on
EXPOSE 80

# Start the container with nginx popping off
CMD ["nginx", "-g", "daemon off;"]
