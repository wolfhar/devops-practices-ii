# Devops Practices II


# Commands

## Database

- docker build -t copa_database_image ./database
- docker run --name copa_database_container -p 5432:5432 -v "%cd%\database\volume:/var/lib/postgresql/data" -e POSTGRES_PASSWORD=copassword -d copa_database_image:latest

## Website

- docker build -t copa_website_image .
- docker run --name copa_website_container -p 80:80 -d copa_website_image:latest





